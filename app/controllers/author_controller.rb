module Api::V1

  class AuthorController < ApplicationController
    def index
      authors = ["Joao Silva", "Luis Guimaraes", "Joao Silva Neto", "Joao Neto", "Celso de Araujo"].to_json
      json_response(@authors)
    end

    private

    def author_params
      params.permit(:first_name)
    end
  end
end